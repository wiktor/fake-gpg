# Fake GPG

Emulates a minimal set of command line options to provide GnuPG-like interface for other applications.

## Prerequisites

This application needs two other services running:

  - Certificate Store: https://gitlab.com/wiktor/minics Set `CERT_STORE_ROOT` to `http://localhost:3000`
  - Private Key Store: https://gitlab.com/wiktor/pks-agent/ and a target backend: https://gitlab.com/sequoia-pgp/pks-openpgp-card set `PKS_AGENT_SOCK` to `/run/user/1000/pks-agent.sock`

## Supported applications

The following applications are supported:

### BrowserPass

Build the binary and set it in BrowserPass configuration file:


```sh
$ cat ~/.password-store/.browserpass.json
{
    "gpgPath": "/home/wiktor/src/pep/fake-gpg/target/release/fake-gpg"
}
```
