use clap::Parser;
use openpgp::policy::StandardPolicy;

use std::io;

use openpgp::cert::prelude::*;
use openpgp::crypto::SessionKey;
use openpgp::parse::{stream::*, Parse};
use openpgp::types::SymmetricAlgorithm;
use sequoia_openpgp as openpgp;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    #[clap(long)]
    decrypt: bool,

    #[clap(long)]
    yes: bool,

    #[clap(long)]
    quiet: bool,

    #[clap(long)]
    batch: bool,

    #[clap(required = true)]
    files: Vec<String>,
}

impl Args {
    pub fn is_decrypt_required(&self) -> bool {
        self.decrypt
            && self.yes
            && self.quiet
            && self.batch
            && self.files.len() == 1
            && self.files[0] == "-"
    }
}

fn main() -> openpgp::Result<()> {
    let args = Args::parse();

    if args.is_decrypt_required() {
        let helper = Helper {};

        // Now, create a decryptor with a helper using the given Certs.
        let policy = &StandardPolicy::new();
        let mut decryptor =
            DecryptorBuilder::from_reader(io::stdin())?.with_policy(policy, None, helper)?;

        // Decrypt the data.
        io::copy(&mut decryptor, &mut io::stdout())?;
        Ok(())
    } else {
        panic!("Unsupported operation: {:?}", args);
    }
}

struct Helper;

impl VerificationHelper for Helper {
    fn get_certs(&mut self, _ids: &[openpgp::KeyHandle]) -> openpgp::Result<Vec<openpgp::Cert>> {
        Ok(Vec::new())
    }

    fn check(&mut self, _structure: MessageStructure) -> openpgp::Result<()> {
        Ok(())
    }
}

impl DecryptionHelper for Helper {
    fn decrypt<D>(
        &mut self,
        pkesks: &[openpgp::packet::PKESK],
        _skesks: &[openpgp::packet::SKESK],
        sym_algo: Option<SymmetricAlgorithm>,
        mut decrypt: D,
    ) -> openpgp::Result<Option<openpgp::Fingerprint>>
    where
        D: FnMut(SymmetricAlgorithm, &SessionKey) -> bool,
    {
        let client = reqwest::blocking::Client::new();
        for pkesk in pkesks {
            let keyid = pkesk.recipient();

            let key = CertParser::from_bytes(
                &client
                    .get(format!(
                        "{}/{:X}",
                        std::env::var("CERT_STORE_ROOT").expect("CERT_STORE_ROOT is set"),
                        keyid
                    ))
                    .send()?
                    .bytes()?,
            )?
            .filter_map(|result| result.ok())
            .flat_map(|cert| {
                cert.keys()
                    .filter(|key| key.keyid() == *keyid)
                    .map(|key| key.key().clone())
                    .collect::<Vec<_>>()
            })
            .next();
            if let Some(key) = key {
                let mut decryptor = fake_gpg::unlock_decryptor(
                    std::env::var("PKS_AGENT_SOCK").expect("PKS_AGENT_SOCK is set"),
                    key.clone(),
                    &String::new().into(),
                )?;

                pkesk
                    .decrypt(decryptor.as_mut(), sym_algo)
                    .map(|(algo, session_key)| decrypt(algo, &session_key));
            }
        }

        Ok(None)
    }
}
